import requests
from bs4 import BeautifulSoup
url_source = 'https://the-internet.herokuapp.com/context_menu'

def scrap_source():
    # Scrap Text from url
    response = requests.get(url_source).text
    # Parse Text with BeautifulSoup
    soup = BeautifulSoup(response, 'html.parser')

    # Extract  Body Text from URL
    test_src = soup.find('body').text

    return test_src

# git push -uf origin main